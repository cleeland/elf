# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Module to handle install of CRC application. """

import os

from common import CRC_DIR_BASE

from common import CRC_DIR_PROF
from common import CRC_DIR_PROF_ACTIVE
from common import CRC_DIR_PROF_OVR
from common import CRC_DIR_PROF_SAVED

from common import CRC_DIR_MANUAL

from common import CRC_DIR_LOG
from common import CRC_DIR_LOG_CSV
from common import CRC_DIR_LOG_DATA

from common import CRC_FILE_MODE

# CRC directories
CRC_APP_DIRS = list()
CRC_APP_DIRS.append(CRC_DIR_BASE)
CRC_APP_DIRS.append(CRC_DIR_PROF)
CRC_APP_DIRS.append(CRC_DIR_PROF_ACTIVE)
CRC_APP_DIRS.append(CRC_DIR_PROF_OVR)
CRC_APP_DIRS.append(CRC_DIR_PROF_SAVED)
CRC_APP_DIRS.append(CRC_DIR_MANUAL)
CRC_APP_DIRS.append(CRC_DIR_LOG)
CRC_APP_DIRS.append(CRC_DIR_LOG_CSV)
CRC_APP_DIRS.append(CRC_DIR_LOG_DATA)

# CRC Files
CRC_FILE_MODE = os.path.join(CRC_DIR_BASE, "mode")

CRC_APP_FILES = list()
CRC_APP_FILES.append(CRC_FILE_MODE)

def crc_installed():
    """ Determine if CRC app is installed. """
    if not os.path.isdir(CRC_DIR_BASE):
        print "crc app not installed. Run 'python install.py' to install."
        return False

    return True

def install_crc_app():
    """Install the needed CRC app directories and files"""
    if not os.path.isdir(CRC_DIR_BASE):

        # Create the needed directories
        print "crc base directory not found"
        print "...create crc directories"
        for appdir in CRC_APP_DIRS:
            print "......creating directory %s" % appdir
            os.makedirs(appdir)

        print "...create crc files"
        for appfile in CRC_APP_FILES:
            print "......creating file %s" % appfile
            modefile = open(CRC_FILE_MODE, 'w+')
            modefile.close()

if __name__ == "__main__":
    install_crc_app()
