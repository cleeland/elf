# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""Module that contains common constants that are used throughout the project.
"""

import os
import platform

WINDOWS = platform.system() == "Windows"

# CRC Directories
CRC_DIR_BASE = os.path.normpath("/home/pi/crc")

CRC_DIR_PROF = os.path.normpath(CRC_DIR_BASE + "/profile")
CRC_DIR_PROF_ACTIVE = os.path.normpath(CRC_DIR_PROF + "/active")
CRC_DIR_PROF_OVR = os.path.normpath(CRC_DIR_PROF + "/override")
CRC_DIR_PROF_SAVED = os.path.normpath(CRC_DIR_PROF + "/saved")

CRC_DIR_MANUAL = os.path.normpath(CRC_DIR_BASE + "/manual")

CRC_DIR_LOG = os.path.normpath(CRC_DIR_BASE + "/log")
CRC_DIR_LOG_CSV = os.path.normpath(CRC_DIR_LOG  + "/csv")
CRC_DIR_LOG_DATA = os.path.normpath(CRC_DIR_LOG  + "/data")

CRC_DIR_APP = os.path.normpath(CRC_DIR_BASE + "/app")

# CRC Files
CRC_FILE_MODE = os.path.join(CRC_DIR_BASE, "mode")

# CRC Modes
CRC_MODE_IDLE = "idle"
CRC_MODE_MANUAL = "manual"
CRC_MODE_PROFILE = "profile"
