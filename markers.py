# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Class the handles the creation of markers for roasts. """
import os

from common import CRC_DIR_LOG

MARKERS_LOG_NAME = "markers"

MAX_MARKERS = 10

class Markers(object):
    """ Class that handles roast markers. """
    def __init__(self):
        pass

    def start(self):
        """ Indicate start of marking capability. """
        log_file = open(self.markers_file(), "w")
        log_file.close()

    def stop(self):
        """ Indicate stop of marking capability. """
        os.remove(self.markers_file())

    def add_marker(self, time):
        """ Add a marker """
        markers_file = open(self.markers_file(), "a")
        markers_file.write(time + "\n")
        markers_file.close()

    def copy_markers(self, dest_file):
        """ Copy markers to the desired file """
        marker_file = open(self.markers_file(), "r")
        dest_file.write(marker_file.read())
        marker_file.close()

    def get_markers(self):
        """ Get the list of markers. """
        markers = ''

        if os.path.isfile(self.markers_file()):
            marker_file = open(self.markers_file(), "r")

            for line in marker_file:
                line = line.rstrip('\n')
                markers += line + ','

            marker_file.close()

        # Remove last comma
        if len(markers) > 0:
            markers = markers[:-1]

        return markers

    def markers_file(self):
        """ Return the markers file handle """
        file_path = os.path.join(CRC_DIR_LOG, MARKERS_LOG_NAME)

        return file_path
