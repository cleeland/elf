# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import os
import time
from common import *
from mode import *
from logger import Logger
from manual import ManualManager
from profile import ProfileManager

# Active Mode
active_mode = None

prof_mgr = ProfileManager()
man_mgr = ManualManager()
logger = Logger()

while(True):
    # Get the active mode
    active_mode = get_mode()

    # Get the list of saved profiles
    saved_profiles = prof_mgr.get_saved_profiles()

    print "\nCurrent Mode: %s" % active_mode

    num_profiles = 0
    for profile in saved_profiles:
        print "%d: %s" % (num_profiles, profile)
        num_profiles += 1

    user_selection = raw_input('Enter selection (q to quit, m for manual) ')

    if user_selection.upper() == 'M':
        switch_mode(CRC_MODE_MANUAL)
        continue

    if user_selection.upper() == 'S':
        switch_mode(CRC_MODE_STOPPED)
        continue

    if user_selection.upper() == 'U':
        man_mgr.change_temp(20)
        continue

    if user_selection.upper() == 'D':
        man_mgr.change_temp(-20)
        continue

    if user_selection.upper() == 'L':
        if logger.log_started():
            logger.stop_log()
        else:
            logger.start_log()
        continue

    if user_selection.upper() == 'Q':
        break

    try:
        user_selection = int( user_selection )
    except ValueError:
        print "Not a valid selection. Try again"
        continue

    if user_selection >= len(saved_profiles):
        print "Not a valid profile. Try again."
        continue

    switch_mode(CRC_MODE_PROFILE)

    prof_mgr.activate_profile( saved_profiles[user_selection] )
