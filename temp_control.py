# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Control the temperature of the roaster """
import math
import random

from common import WINDOWS
import heater

TEMP_INVALID = 999

# Only do active temperature control when
# running on the raspberry pi.
if not WINDOWS:
    import thermo

def heat_off():
    """ Turn heater off """
    heater.heat_off()

def get_temp(temp):
    """ Get the temperature reading from the thermocouple """
    # Add some randomness to make the gauge move on
    # windows.
    adjust = random.uniform(1, 20)
    if adjust < 10:
        adjust = -adjust
    else:
        adjust = adjust - 10

    current_temp = temp + adjust

    # Get the actual thermocouple reading if running on the
    # raspberry pi platform.
    if not WINDOWS:
        current_temp = thermo.get_temp_f()

    return current_temp

def set_temp(temp):
    """ Attempt to set the temperature to the desired value. """
    current_temp = get_temp(temp)

    # Occasionally a bad temp reading occurs.
    if math.isnan(current_temp):
        return TEMP_INVALID

    if current_temp < temp:
        heater.heat_on()
    else:
        heater.heat_off()

    return current_temp
