# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Module to handle state tick when the roaster is idle."""

from command import Command

from common import CRC_DIR_MANUAL

import logger

class IdleManager(object):
    """
    Class to handle the clock ticks when the system
    is idle. Doesn't do much.
    """
    def __init__(self):
        print "Idle, init"
        if Command.cmd_exists(CRC_DIR_MANUAL):
            Command.load(CRC_DIR_MANUAL)
        else:
            self.create_cmd()

        self.logger = logger.Logger()
        self.start_time = 0.0
        self.num_ticks = 0

    def create_cmd(self):
        """ Create an idle command file """
        active_cmd = Command(CRC_DIR_MANUAL)
        active_cmd.activate()

    def set_start(self, start_time):
        """ Set start time """
        self.start_time = float(start_time)
        self.num_ticks = 0

    def tick(self):
        """ Handle tick """
        command = Command.load(CRC_DIR_MANUAL)

        # Compute end_time
        self.num_ticks += 1
        end_time = self.start_time + self.num_ticks

        # Set temp to room temperature for windows env.
        command.heat_off()
        command.read_temp(end_time)
        self.logger.logcmd(command)
