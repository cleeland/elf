# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""This is the crc daemon process that controls the heater based
on the temperature setting. It operates in Idle, Manual, and
Profile mode. Manual allows the operator to manually change
the temperature while Profile is full automation.
"""

from common import CRC_MODE_IDLE, CRC_MODE_MANUAL
from install import crc_installed
from idle import IdleManager
from logger import Logger
from manual import ManualManager
from mode import get_mode, switch_mode

def main():
    """ Main loop for controlling the heater. """

    # See if app is installed
    if not crc_installed():
        return

    # Active Mode
    active_mode = None

    # Current Mode
    current_mode = None

    # Create a manager for idle mode
    idle_mgr = IdleManager()

    # Create a manager for manual mode
    man_mgr = ManualManager()

    # Create a logger for logging
    logger = Logger()

    # Always start in idle mode
    start_time = switch_mode(CRC_MODE_IDLE)
    idle_mgr.set_start(start_time)

    while(True):
        active_mode, start_time = get_mode()

        # If the mode has changed, stop any logs that
        # have been started.
        if active_mode != current_mode:
            if logger.log_started():
                logger.stop_log()

        if active_mode == CRC_MODE_IDLE:
            if current_mode != CRC_MODE_IDLE:
                idle_mgr.set_start(start_time)

            idle_mgr.tick()

        if active_mode == CRC_MODE_MANUAL:
            # Start a new log when switching to manual mode.
            if current_mode != CRC_MODE_MANUAL:
                man_mgr.set_start(start_time)
                logger.start_log()

            man_mgr.tick()

        current_mode = active_mode

main()
