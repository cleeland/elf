This is the repository for the Electric Lights and Fire (ELF) raspberry pi coffee roaster. Please see the license.txt file for licensing information regarding this software.

**External Libraries**

The Adafruit python 31855 and GPIO libraries are needed to talk to the thermocouple board. To install the libraries:
```
#!shell

cd ~
git clone https://github.com/adafruit/Adafruit_Python_MAX31855.git
git clone https://github.com/adafruit/Adafruit_Python_GPIO.git
cd Adafruit_Python_MAX31855
sudo python setup.py install
cd ../Adafruit_Python_GPIO
sudo python setup.py install

```

Django is used to provide the web interface. To install:

```
#!shell

cd ~
sudo apt-get install python-django

```


**Installation**

To install the directories and files needed to run the application:
> python install.py

**Running the Application**

The application consists of two processes: a daemon that controls the temperature and a web server for the user interface. Both have to be started before using the system.
> python crcd.py

> python web\manage.py runserver 0.0.0.0:8054


*Control Panel: (available at localhost:8054/control)*

Alt-S - To start/stop the roaster

Alt-Up - To increase the temperature

Alt-Down - To decrease the temperature

Alt-M - To create a marker at the current time (the blue vertical lines in the image below are markers)

![roasting.png](https://bitbucket.org/repo/8bXzrk/images/1593128147-roasting.png)

*Roast Log (available at localhost:8504/roasts or by clicking on the log icon):*
![log.png](https://bitbucket.org/repo/8bXzrk/images/1009993535-log.png)

Click on the "prof" button to create a profile from a roast log.  This will take you to the profiles page where the profile name, temperatures, and notes can be modified.

*Profiles:*

![profile_edit.png](https://bitbucket.org/repo/8bXzrk/images/157419981-profile_edit.png)

Click "load" to load the profile into the control panel.  The loaded profile name will be shown on the control panel below the notes section. The profile temperatures are shown as the blue line on the chart. Click the stop icon next to the profile name to stop running a profile. The manual temperature adjustments will still work even while the profile is running.

![control_panel_profile_running.png](https://bitbucket.org/repo/8bXzrk/images/20580946-control_panel_profile_running.png)