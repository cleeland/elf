from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = (
    url(r'^control/', include('control.urls')),
    url(r'^profiles/', include('profiles.urls')),
    url(r'^roasts/', include('roasts.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
