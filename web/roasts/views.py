# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from django.http import HttpResponse

from django.shortcuts import render

from logger import Logger
from profile import Profile

from manual import ManualManager
from mode import *
from notes import Notes

import time

import json

def make_profile_data( roast_data ):
    start_time = None
    intv_time = None
    intv_temp = None
    intervals = list()

    for data_point in roast_data:
        data_parts = data_point.split(',')
        curr_time = float(data_parts[0])
        curr_temp = data_parts[1]

        # First data point is the start time.
        if start_time == None:
            start_time = curr_time
            intv_time = 0.0
            intv_temp = curr_temp
            continue

        # New interval is starting. Capture the current interval.
        if intv_temp != curr_temp:
            interval = str(intv_time) + ',' + intv_temp
            intervals.append( interval )

            # Start new interval
            intv_temp  = curr_temp
            intv_time = curr_time - start_time

    # Capture the last interval
    interval = str(intv_time) + ',' + intv_temp
    intervals.append( interval )

    return intervals

# Index page will return blank page.
def index(request):
    return render(request, 'roasts/index.html')

# Get the list of roast files.
def get_roasts(request):
    # Construct list of csv log files
    logger = Logger()
    roasts = logger.get_csv_list()

    context = { 
                'roasts': roasts,
              } 

    return render(request, 'roasts/roasts.html', context)

# Get a particular roast file to display
def get_roast(request, csv_file):

    logger = Logger()
    notes, data, markers = logger.parse_csv_file( csv_file )

    context = { 
                'notes': notes,
                'data': data,
                'markers': markers,
              } 

    return HttpResponse(json.dumps(context), content_type="application/json")

# Get the csv version of the roast file so it can be exported.
def get_roast_file(request, csv_file):

    logger = Logger()
    csv_file = logger.get_csv_file( csv_file )

    return HttpResponse(csv_file, content_type="text/csv")

# Make a roast log into a profile
def make_profile(request, csv_file):
    logger = Logger()
    notes, csv_data, markers = logger.parse_csv_file( csv_file )

    profile_data = make_profile_data( csv_data )

    profiles = Profile()

    profile_name = profiles.make_new_profile( csv_file, notes, profile_data )

    return HttpResponse(profile_name, content_type="text/csv")
