# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from django.http import HttpResponse
from django.shortcuts import render
from logger import Logger
from manual import ManualManager
from profile import Profile
from mode import *
from notes import Notes
from markers import Markers

import time
import json

# Index page will populate the supported list of gauge temps.
def index(request):
    return render(request, 'control/panel.html')

# Get the latest state
def get_state(request):
    # Get the latest command log info which contains the temps.
    logger = Logger()
    cmd = logger.get_cmd_log()

    timestamp = 0.0 
    temp_set = 0
    temp_act = 0

    if cmd:
        last_tick = cmd.last_tick
        temp_set = int(cmd.temp_set)
        temp_act = int(cmd.temp_act)
        current_time = time.time()

    # Get the current system mode.
    mode, start = get_mode()

    # Get the current notes.
    notes = Notes()
    notes_text = notes.get_notes()

    # Get the current list of markers.
    markers = Markers().get_markers()

    context = { 
                'last_tick': last_tick,
                'mode': mode,
                'start': start,
                'notes': notes_text,
                'markers': markers,
                'temp_set': temp_set,
                'temp_act': temp_act, 
                'current_time': current_time,
              } 

    return HttpResponse(json.dumps(context), content_type="application/json")

# Cancel the active profile
def cancel_profile(request):
    profile = Profile()

    profile.cancel_active_profile()

    return HttpResponse()

# Get any active profile
def get_profile(request):
    profile = Profile()

    prof_name, prof_intervals, prof_notes = profile.get_active_profile()

    if ( len(prof_name) > 0 ):
        notes = Notes()
        notes.update_notes( prof_notes )

    context = { 
                'profile_name': prof_name,
                'profile_intervals': prof_intervals,
                'profile_notes': prof_notes,
              } 

    return HttpResponse(json.dumps(context), content_type="application/json")

# Set the temperature in manual mode
def set_temp(request):

    # The new temp setting is sent as part of the POST request.
    new_temp = int(request.POST.get('temp'))

    mm = ManualManager()
    mm.set_temp(new_temp)

    context = { 
                'temp_set': new_temp,
              } 

    return HttpResponse(json.dumps(context), content_type="application/json")

# Set the notes
def set_notes(request):
    # The new notes are sent as part of the POST request.
    new_notes = request.POST.get('notes')

    notes = Notes()

    notes.update_notes( new_notes )

    return HttpResponse()

# Set the mode
def set_mode(request):

    # The new mode is sent as part of the POST request.
    mode = request.POST.get('mode')

    switch_mode(mode)

    return HttpResponse()

# Add a marker
def add_marker(request):
    timestamp = str(int(request.POST.get('timestamp')))

    Markers().add_marker( timestamp )

    return HttpResponse()
