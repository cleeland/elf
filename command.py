# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Command module for the Electric Lights and Fire (ELF) coffee roaster.

This module parses the command file that sets the temperature. It also
writes to the command file, in the proper format, when the temperatur
is modified.

"""

import os
import time
import temp_control

# Location of command fields in the command file
CRC_LINE_LAST_TICK = 0
CRC_LINE_SECS = 1
CRC_LINE_SECS_LEFT = 2
CRC_LINE_FAN = 3
CRC_LINE_TEMP_SET = 4
CRC_LINE_TEMP_ACT = 5

CRC_CMD_FILE = "cmd"

TEMP_MIN = 50
TEMP_MAX = 550

class Command(object):
    """
    The command class encapsulates the data and methods needed
    to read a command from a command file, write to a command
    file and apply the command (i.e. set the temperature of
    the roaster).
    """
    def __init__(self, cmd_dir):
        # Directory where the command file lives.
        self.cmd_dir = cmd_dir
        self.cmd_file = os.path.join(cmd_dir, CRC_CMD_FILE)

        # Logged fields
        self.last_tick = float(0.0)
        self.seconds = int(0)
        self.seconds_left = float(0.0)
        self.fan = float(0.0)
        self.temp_set = float(0.0)
        self.temp_act = float(0.0)

    def copy_vals(self, cmd):
        """ Copy values from another command object """
        self.last_tick = cmd.last_tick
        self.seconds = cmd.seconds
        self.seconds_left = cmd.seconds_left
        self.fan = cmd.fan
        self.temp_set = cmd.temp_set
        self.temp_act = cmd.temp_act

    def set_temp(self, new_temp):
        """ Set the temperature to the new value """
        if (new_temp < TEMP_MIN) or (new_temp > TEMP_MAX):
            return

        self.temp_set = new_temp

        self.update_command_file()

    def change_temp(self, amount):
        """ Increment the temperature by the passed in amount """
        self.temp_set += amount

        if self.temp_set < 0:
            self.temp_set = 0
        self.update_command_file()

    def parse(self, line):
        """ Parse a line as a command """
        # Remove LF from line string
        if line[-1] == '\n':
            line = line[:-1]

        line_parts = line.split(',')

        if len(line_parts) == 6:
            self.last_tick = float(line_parts[CRC_LINE_LAST_TICK])
            self.seconds = int(line_parts[CRC_LINE_SECS])
            self.seconds_left = float(self.seconds)
            self.fan = float(line_parts[CRC_LINE_FAN])
            self.temp_set = float(line_parts[CRC_LINE_TEMP_SET])
            self.temp_act = float(line_parts[CRC_LINE_TEMP_ACT])
            return True
        else:
            return False

    def activate(self):
        """ Make this command active """
        self.seconds_left = float(self.seconds)
        self.update_command_file()

    def purge(self):
        """ Command is no longer needed. """
        if os.path.isfile(self.cmd_file):
            os.remove(self.cmd_file)

    def tick(self):
        """ Tick of the clock. Called every second. """
        tick_start = time.time()
        tick_end = tick_start + 1
        self.apply(tick_end)
        self.seconds_left -= (tick_end - tick_start)
        self.update_command_file()
        print "cmd tick, seconds left: %f" % self.seconds_left

    def apply(self, end_time):
        """ Apply the command until the given end time. """
        print "cmd apply, temp set: %f" % self.temp_set
        self.last_tick = end_time
        while time.time() < end_time:
            # Set temp returns the current temperature
            temp_reading = temp_control.set_temp(self.temp_set)

            # Sometimes the thermocouple produces a bad reading.
            if temp_reading != temp_control.TEMP_INVALID:
                self.temp_act = temp_reading
            time.sleep(0.1)

    def heat_off(self):
        """ Turn off heat. """
        temp_control.heat_off()

    def read_temp(self, end_time):
        """ Read temperature from thermocouple """
        self.last_tick = end_time
        while time.time() < end_time:
            # For windows, use room temp (70). On PI
            # the actual temperature will be returned.
            temp_reading = temp_control.get_temp(70)
            if temp_reading != temp_control.TEMP_INVALID:
                self.temp_act = temp_reading
            time.sleep(0.1)

    def update_command_file(self):
        """ Update the command file with the values of this command. """
        cmd_file = open(self.cmd_file, "w")
        cmd_file.write(self.to_string())
        cmd_file.close()

    def is_done(self):
        """ Determine if this command is finished. """
        if self.seconds_left <= 0:
            self.purge()
            return True

        return False

    def to_string(self):
        """ String output for command """
        cmd_str = "%f,%d,%d,%f,%f,%f\n" % (self.last_tick,
                                           self.seconds,
                                           self.seconds_left,
                                           self.fan,
                                           self.temp_set,
                                           self.temp_act)
        return cmd_str

    @staticmethod
    def cmd_exists(cmd_dir):
        """ Does a command file exist in the given directory. """
        return os.path.isfile(os.path.join(cmd_dir, CRC_CMD_FILE))

    @staticmethod
    def load(cmd_dir):
        """ Load the command file from the given directory. """
        cmd_file_path = os.path.join(cmd_dir, CRC_CMD_FILE)

        if os.path.isfile(cmd_file_path) is False:
            return None

        cmd_file = open(cmd_file_path, "r")
        line = cmd_file.readline()

        active_cmd = Command(cmd_dir)
        active_cmd.parse(line)
        return active_cmd
