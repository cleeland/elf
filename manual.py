# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Control behavior of roaster in manual mode. """

from command import Command
from common import CRC_DIR_MANUAL
import logger

class ManualManager(object):
    """ Manages manual operation of the ELF. """
    def __init__(self):
        if Command.cmd_exists(CRC_DIR_MANUAL):
            Command.load(CRC_DIR_MANUAL)
        else:
            self.create_cmd()

        self.logger = logger.Logger()
        self.start_time = 0.0
        self.num_ticks = 0

    def apply_cmd(self, cmd):
        """ Apply the passed in command. """
        active_cmd = Command(CRC_DIR_MANUAL)
        active_cmd.copy_vals(cmd)
        active_cmd.activate()

    def create_cmd(self):
        """ Create a new command for manual operation. """
        active_cmd = Command(CRC_DIR_MANUAL)
        active_cmd.activate()

    def change_temp(self, amount):
        """ Change the temp by the specified amount. """
        command = Command.load(CRC_DIR_MANUAL)
        command.change_temp(amount)

    def set_temp(self, new_temp):
        """ Set the temp to the desired value. """
        command = Command.load(CRC_DIR_MANUAL)
        command.set_temp(new_temp)

    def set_start(self, start_time):
        """ Set the start time of the roast. """
        self.start_time = float(start_time)
        self.num_ticks = 0

    def tick(self):
        """ Process the 1-second tick of the ELF. """
        command = Command.load(CRC_DIR_MANUAL)
        self.num_ticks += 1
        end_time = self.start_time + self.num_ticks
        command.apply(end_time)
        self.logger.logcmd(command)
