# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Class to handle the creation and editing of the notes portion. """
import os

from common import CRC_DIR_LOG

NOTES_LOG_NAME = "notes"

class Notes(object):
    """ Class that handles roast notes. """
    def __init__(self):
        pass

    def update_notes(self, notes):
        """ Update the notes for the roast. """
        # always update the default notes file
        self.update_notes_file(NOTES_LOG_NAME, notes)

    def get_notes(self):
        """ Get the last logged notes. """
        note_file = open(self.notes_file())
        notes = note_file.read()
        note_file.close()

        return notes

    def update_notes_file(self, log_file, notes):
        """ Update the notes file """
        log_file = open(os.path.join(CRC_DIR_LOG, log_file), "w+")
        log_file.write(notes)
        log_file.close()

    def notes_file(self):
        """ Return the notes file handle """
        file_path = os.path.join(CRC_DIR_LOG, NOTES_LOG_NAME)

        # Create notes file if it doesn't exist.
        if not os.path.isfile(file_path):
            log_file = open(file_path, "w")
            log_file.close()

        return file_path
