# Copyright (c) 2016
# Author: Mark Sanders
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
""" Class to run a roast profile. """
import os
from shutil import copyfile

from common import CRC_DIR_PROF_ACTIVE
from common import CRC_DIR_PROF_SAVED

PROF_DATA_START = "PROF_DATA\n"

class ProfileFile(object):
    """ Profile file """
    def __init__(self):
        self.file_name = ''
        self.modify_time_secs = None

class Profile(object):
    """ Handle profile operations """
    def __init__(self):
        pass

    def make_new_profile(self, csv_file, notes, profile_data):
        """ Make a new profile """
        # Remove file extension
        file_path = None
        csv_file = csv_file[:-4]
        prof_file = csv_file
        num_tries = 1

        # Make a new unique filename by adding _X to existing
        # files
        while True:
            file_path = os.path.join(CRC_DIR_PROF_SAVED, prof_file + ".prof")
            if not os.path.isfile(file_path):
                break
            else:
                prof_file = csv_file + "_%d" % num_tries
                num_tries = num_tries + 1

        try:
            new_prof_file = open(file_path, "w")
        except:
            print "Unable to update profile %s" % file_path
            return False

        self.write_profile(new_prof_file, profile_data, notes)
        new_prof_file.close()

        return  prof_file + ".prof"

    def write_profile(self, prof_file, data, notes):
        """ Write a profile """
        prof_file.write(notes)
        prof_file.write('\n')
        prof_file.write(PROF_DATA_START)
        for temp_int in data:
            prof_file.write(temp_int)
            prof_file.write('\n')

    def get_profiles(self):
        """ Get the list of profiles """
        profile_files = list()
        file_list = os.listdir(CRC_DIR_PROF_SAVED)
        for profile_file in file_list:
            file_path = os.path.join(CRC_DIR_PROF_SAVED, profile_file)
            if os.path.isfile(file_path):
                modify_time_secs = os.path.getmtime(file_path)

                roast_file = ProfileFile()
                roast_file.file_name = profile_file[:-5]
                roast_file.modify_time_secs = modify_time_secs
                profile_files.append(roast_file)

        return sorted(profile_files, key=lambda profile: profile.file_name)

    def delete_profile(self, profile_file):
        """ Delete a profile """
        # Add profile extension back
        profile_file = profile_file + ".prof"
        if os.path.isfile(os.path.join(CRC_DIR_PROF_SAVED, profile_file)):
            os.remove(os.path.join(CRC_DIR_PROF_SAVED, profile_file))

    def parse_profile_file(self, profile_file):
        """ Parse a profile file so the data can be displayed
            on the web page """
        notes = ''
        data = list()
        data_found = False

        # Add profile extension back
        profile_file = profile_file + ".prof"
        try:
            if os.path.isfile(os.path.join(CRC_DIR_PROF_SAVED, profile_file)):
                prof_file = open(os.path.join(CRC_DIR_PROF_SAVED, profile_file))

                for line in prof_file:
                    # Found the data marker. Start parsing data entries.
                    if line == PROF_DATA_START:
                        data_found = True
                    else:
                        # Parsing notes until data keyword found.
                        if data_found is False:
                            notes += line
                        else:
                            line = line.rstrip('\n')
                            data.append(line)

        except:
            print "Unable to parse %s profile file" % profile_file
            return notes, data

        return notes, data

    def clean_active_directory(self):
        """ Clean out the active directory """
        file_list = os.listdir(CRC_DIR_PROF_ACTIVE)

        for profile_file in file_list:
            file_path = os.path.join(CRC_DIR_PROF_ACTIVE, profile_file)
            if os.path.isfile(file_path):
                os.remove(file_path)

    def cancel_active_profile(self):
        """ Cancel a profile from running. """
        self.clean_active_directory()

    def get_active_profile(self):
        """ Get the active profile """
        name = ''
        notes = ''
        data = ''
        file_list = os.listdir(CRC_DIR_PROF_ACTIVE)

        for profile_file in file_list:
            name = profile_file[:-5]
            notes, data = self.parse_profile_file(name)

        return name, data, notes


    def load_profile(self, profile_name):
        """ Load a profile """
        self.clean_active_directory()

        src_file = os.path.join(CRC_DIR_PROF_SAVED, profile_name + '.prof')
        dest_file = os.path.join(CRC_DIR_PROF_ACTIVE, profile_name + '.prof')
        copyfile(src_file, dest_file)

    def change_profile_name(self, old_profile_name, new_profile_name):
        """ Change the name of a profile """
        if os.path.isfile(os.path.join(CRC_DIR_PROF_SAVED, old_profile_name)):
            os.rename(os.path.join(CRC_DIR_PROF_SAVED, old_profile_name),
                       os.path.join(CRC_DIR_PROF_SAVED, new_profile_name))

    def save_profile(self, profile_name, new_profile_name, data, notes):
        """ Save a profile """
        file_path = os.path.join(CRC_DIR_PROF_SAVED, profile_name + '.prof')

        if os.path.isfile(file_path):
            # Data is a ';' separated list of temp settings. Convert into
            # list
            temps = list()
            intervals = data.split(';')
            for interval in intervals:
                if len(interval) > 0:
                    temps.append(interval)
            prof_file = open(file_path, "w")
            self.write_profile(prof_file, temps, notes)
            prof_file.close()

        if profile_name != new_profile_name:
            self.change_profile_name(profile_name + '.prof', new_profile_name + '.prof')
